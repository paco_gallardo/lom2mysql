"""
This file has been automatically generated with workbench_alchemy v0.9.2
For more details please check here:
https://github.com/PiTiLeZarD/workbench_alchemy
"""

# USE_MYSQL_TYPES = False
# try:
#	 from . import USE_MYSQL_TYPES
# except:
#	 pass


from sqlalchemy.orm import relationship
from sqlalchemy import Column, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Integer, DateTime, String

# if USE_MYSQL_TYPES:
# else:

Base = declarative_base()

class Lom(Base):
	__tablename__ = 'loms'
	
	id = Column( "lom_id", Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )


	def __init__(self
		, created_at_
		, updated_at_
 ):

		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<Lom %(id)s>' % self.__dict__

class General(Base):
	__tablename__ = 'generals'
	
	general_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	lom_id = Column( Integer, ForeignKey("loms.lom_id"), nullable=False, primary_key=True, autoincrement=False, unique=True )
	general_structure = Column( String(1000), nullable=False )
	general_structure_source = Column( String(1000) )
	general_aggregation_level = Column( String(1000), nullable=False )
	general_aggregation_level_source = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	lom = relationship( "Lom", foreign_keys=[lom_id], backref="generals" )

	def __init__(self
		, lom_id_
		, general_structure_
		, general_structure_source_
		, general_aggregation_level_
		, general_aggregation_level_source_
		, created_at_
		, updated_at_
 ):

		self.lom_id = lom_id_
		self.general_structure = general_structure_
		self.general_structure_source = general_structure_source_
		self.general_aggregation_level = general_aggregation_level_
		self.general_aggregation_level_source = general_aggregation_level_source_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<General %(lom_id)s %(general_id)s>' % self.__dict__

class Relation(Base):
	__tablename__ = 'relations'
	
	relation_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	lom_id = Column( Integer, ForeignKey("loms.lom_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	relation_kind = Column( String(1000) )
	relation_kind_source = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	lom = relationship( "Lom", foreign_keys=[lom_id], backref="relations" )

	def __init__(self
		, lom_id_
		, relation_kind_
		, relation_kind_source_
		, created_at_
		, updated_at_
 ):

		self.lom_id = lom_id_
		self.relation_kind = relation_kind_
		self.relation_kind_source = relation_kind_source_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<Relation %(lom_id)s %(relation_id)s>' % self.__dict__

class Annotation(Base):
	__tablename__ = 'annotations'
	
	annotation_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	lom_id = Column( Integer, ForeignKey("loms.lom_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	annotation_entity = Column( String(1000) )
	annotation_date = Column( DateTime )
	annotation_date_description = Column( String )
	annotation_date_description_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	lom = relationship( "Lom", foreign_keys=[lom_id], backref="annotations" )

	def __init__(self
		, lom_id_
		, annotation_entity_
		, annotation_date_
		, annotation_date_description_
		, annotation_date_description_lang_
		, created_at_
		, updated_at_
 ):

		self.lom_id = lom_id_
		self.annotation_entity = annotation_entity_
		self.annotation_date = annotation_date_
		self.annotation_date_description = annotation_date_description_
		self.annotation_date_description_lang = annotation_date_description_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<Annotation %(lom_id)s %(annotation_id)s>' % self.__dict__

class Lifecycle(Base):
	__tablename__ = 'lifecycles'
	
	lifecycle_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	lom_id = Column( Integer, ForeignKey("loms.lom_id"), nullable=False, primary_key=True, autoincrement=False, unique=True )
	lifecycle_status = Column( String(1000) )
	lifecycle_status_source = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	lom = relationship( "Lom", foreign_keys=[lom_id], backref="lifecycles" )

	def __init__(self
		, lom_id_
		, lifecycle_status_
		, lifecycle_status_source_
		, created_at_
		, updated_at_
 ):

		self.lom_id = lom_id_
		self.lifecycle_status = lifecycle_status_
		self.lifecycle_status_source = lifecycle_status_source_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<Lifecycle %(lifecycle_id)s %(lom_id)s>' % self.__dict__

class Metametadata(Base):
	__tablename__ = 'metametadatas'
	
	metametadata_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	lom_id = Column( Integer, ForeignKey("loms.lom_id"), nullable=False, primary_key=True, autoincrement=False, unique=True )
	metametadata_lang = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	lom = relationship( "Lom", foreign_keys=[lom_id], backref="metametadatas" )

	def __init__(self
		, lom_id_
		, metametadata_lang_
		, created_at_
		, updated_at_
 ):

		self.lom_id = lom_id_
		self.metametadata_lang = metametadata_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<Metametadata %(lom_id)s %(metametadata_id)s>' % self.__dict__

class Classification(Base):
	__tablename__ = 'classifications'
	
	classification_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	lom_id = Column( Integer, ForeignKey("loms.lom_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	classification_purpose = Column( String(1000) )
	classification_purpose_source = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	lom = relationship( "Lom", foreign_keys=[lom_id], backref="classifications" )

	def __init__(self
		, lom_id_
		, classification_purpose_
		, classification_purpose_source_
		, created_at_
		, updated_at_
 ):

		self.lom_id = lom_id_
		self.classification_purpose = classification_purpose_
		self.classification_purpose_source = classification_purpose_source_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<Classification %(classification_id)s %(lom_id)s>' % self.__dict__

class Technical(Base):
	__tablename__ = 'technicals'
	
	technical_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	lom_id = Column( Integer, ForeignKey("loms.lom_id"), nullable=False, primary_key=True, autoincrement=False, unique=True )
	technical_size = Column( String(30) )
	technical_duration = Column( String(200) )
	technical_duration_description = Column( String )
	technical_duration_description_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	lom = relationship( "Lom", foreign_keys=[lom_id], backref="technicals" )

	def __init__(self
		, lom_id_
		, technical_size_
		, technical_duration_
		, technical_duration_description_
		, technical_duration_description_lang_
		, created_at_
		, updated_at_
 ):

		self.lom_id = lom_id_
		self.technical_size = technical_size_
		self.technical_duration = technical_duration_
		self.technical_duration_description = technical_duration_description_
		self.technical_duration_description_lang = technical_duration_description_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<Technical %(lom_id)s %(technical_id)s>' % self.__dict__

class Educational(Base):
	__tablename__ = 'educationals'
	
	educational_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	lom_id = Column( Integer, ForeignKey("loms.lom_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	educational_interactivitytype = Column( String(1000) )
	educational_interactivitytype_source = Column( String(1000) )
	educational_interactivitylevel = Column( String(1000) )
	educational_interactivitylevel_source = Column( String(1000) )
	educational_semanticdensity = Column( String(1000) )
	educational_semanticdensity_source = Column( String(1000) )
	educational_difficulty = Column( String(1000) )
	educational_difficulty_source = Column( String(1000) )
	educational_typicallearningtime = Column( String(200) )
	educational_typicallearningtime_description = Column( String )
	educational_typicallearningtime_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	lom = relationship( "Lom", foreign_keys=[lom_id], backref="educationals" )

	def __init__(self
		, lom_id_
		, educational_interactivitytype_
		, educational_interactivitytype_source_
		, educational_interactivitylevel_
		, educational_interactivitylevel_source_
		, educational_semanticdensity_
		, educational_semanticdensity_source_
		, educational_difficulty_
		, educational_difficulty_source_
		, educational_typicallearningtime_
		, educational_typicallearningtime_description_
		, educational_typicallearningtime_lang_
		, created_at_
		, updated_at_
 ):

		self.lom_id = lom_id_
		self.educational_interactivitytype = educational_interactivitytype_
		self.educational_interactivitytype_source = educational_interactivitytype_source_
		self.educational_interactivitylevel = educational_interactivitylevel_
		self.educational_interactivitylevel_source = educational_interactivitylevel_source_
		self.educational_semanticdensity = educational_semanticdensity_
		self.educational_semanticdensity_source = educational_semanticdensity_source_
		self.educational_difficulty = educational_difficulty_
		self.educational_difficulty_source = educational_difficulty_source_
		self.educational_typicallearningtime = educational_typicallearningtime_
		self.educational_typicallearningtime_description = educational_typicallearningtime_description_
		self.educational_typicallearningtime_lang = educational_typicallearningtime_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<Educational %(educational_id)s %(lom_id)s>' % self.__dict__

class Right(Base):
	__tablename__ = 'rights'
	
	right_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	lom_id = Column( Integer, ForeignKey("loms.lom_id"), nullable=False, primary_key=True, autoincrement=False, unique=True )
	right_cost = Column( String(1000) )
	right_cost_source = Column( String(1000) )
	right_copyright = Column( String(1000) )
	right_copyright_source = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	lom = relationship( "Lom", foreign_keys=[lom_id], backref="rights" )

	def __init__(self
		, lom_id_
		, right_cost_
		, right_cost_source_
		, right_copyright_
		, right_copyright_source_
		, created_at_
		, updated_at_
 ):

		self.lom_id = lom_id_
		self.right_cost = right_cost_
		self.right_cost_source = right_cost_source_
		self.right_copyright = right_copyright_
		self.right_copyright_source = right_copyright_source_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<Right %(right_id)s %(lom_id)s>' % self.__dict__

class Resource(Base):
	__tablename__ = 'resources'
	
	resource_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	relation_id = Column( Integer, ForeignKey("relations.relation_id"), nullable=False, primary_key=True, autoincrement=False, unique=True )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	relation = relationship( "Relation", foreign_keys=[relation_id], backref="resources" )

	def __init__(self
		, relation_id_
		, created_at_
		, updated_at_
 ):

		self.relation_id = relation_id_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<Resource %(relation_id)s %(resource_id)s>' % self.__dict__

class MetametadatasContribute(Base):
	__tablename__ = 'metametadatas_contributes'
	
	metametadatas_contribute_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	metametadata_id = Column( Integer, ForeignKey("metametadatas.metametadata_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	contribute_role = Column( String(1000) )
	contribute_role_source = Column( String(1000) )
	contribute_date = Column( DateTime )
	contribute_date_description = Column( String )
	contribute_date_description_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	metametadata = relationship( "Metametadata", foreign_keys=[metametadata_id], backref="metametadatasContributes" )

	def __init__(self
		, metametadata_id_
		, contribute_role_
		, contribute_role_source_
		, contribute_date_
		, contribute_date_description_
		, contribute_date_description_lang_
		, created_at_
		, updated_at_
 ):

		self.metametadata_id = metametadata_id_
		self.contribute_role = contribute_role_
		self.contribute_role_source = contribute_role_source_
		self.contribute_date = contribute_date_
		self.contribute_date_description = contribute_date_description_
		self.contribute_date_description_lang = contribute_date_description_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<MetametadatasContribute %(metametadatas_contribute_id)s %(metametadata_id)s>' % self.__dict__

class Requirement(Base):
	__tablename__ = 'requirements'
	
	requirement_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	technical_id = Column( Integer, ForeignKey("technicals.technical_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	technical = relationship( "Technical", foreign_keys=[technical_id], backref="requirements" )

	def __init__(self
		, technical_id_
		, created_at_
		, updated_at_
 ):

		self.technical_id = technical_id_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<Requirement %(requirement_id)s %(technical_id)s>' % self.__dict__

class Orcomposite(Base):
	__tablename__ = 'orcomposites'
	
	orcomposite_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	requirement_id = Column( Integer, ForeignKey("requirements.requirement_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	orcomposite_type = Column( String(1000) )
	orcomposite_type_source = Column( String(1000) )
	orcomposite_name = Column( String(1000) )
	orcomposite_name_source = Column( String(1000) )
	orcomposite_minimumversion = Column( String(30) )
	orcomposite_maximumversion = Column( String(30) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	requirement = relationship( "Requirement", foreign_keys=[requirement_id], backref="orcomposites" )

	def __init__(self
		, requirement_id_
		, orcomposite_type_
		, orcomposite_type_source_
		, orcomposite_name_
		, orcomposite_name_source_
		, orcomposite_minimumversion_
		, orcomposite_maximumversion_
		, created_at_
		, updated_at_
 ):

		self.requirement_id = requirement_id_
		self.orcomposite_type = orcomposite_type_
		self.orcomposite_type_source = orcomposite_type_source_
		self.orcomposite_name = orcomposite_name_
		self.orcomposite_name_source = orcomposite_name_source_
		self.orcomposite_minimumversion = orcomposite_minimumversion_
		self.orcomposite_maximumversion = orcomposite_maximumversion_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<Orcomposite %(requirement_id)s %(orcomposite_id)s>' % self.__dict__

class Taxonpath(Base):
	__tablename__ = 'taxonpaths'
	
	taxonpath_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	classification_id = Column( Integer, ForeignKey("classifications.classification_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	classification = relationship( "Classification", foreign_keys=[classification_id], backref="taxonpaths" )

	def __init__(self
		, classification_id_
		, created_at_
		, updated_at_
 ):

		self.classification_id = classification_id_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<Taxonpath %(classification_id)s %(taxonpath_id)s>' % self.__dict__

class Taxon(Base):
	__tablename__ = 'taxons'
	
	taxon_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	taxonpath_id = Column( Integer, ForeignKey("taxonpaths.taxonpath_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	taxon_id_string = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	taxonpath = relationship( "Taxonpath", foreign_keys=[taxonpath_id], backref="taxons" )

	def __init__(self
		, taxonpath_id_
		, taxon_id_string_
		, created_at_
		, updated_at_
 ):

		self.taxonpath_id = taxonpath_id_
		self.taxon_id_string = taxon_id_string_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<Taxon %(taxon_id)s %(taxonpath_id)s>' % self.__dict__

class GeneralsLanguage(Base):
	__tablename__ = 'generals_languages'
	
	generals_language_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	general_id = Column( Integer, ForeignKey("generals.general_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	generals_language_text = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	general = relationship( "General", foreign_keys=[general_id], backref="generalsLanguages" )

	def __init__(self
		, general_id_
		, generals_language_text_
		, created_at_
		, updated_at_
 ):

		self.general_id = general_id_
		self.generals_language_text = generals_language_text_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<GeneralsLanguage %(general_id)s %(generals_language_id)s>' % self.__dict__

class GeneralsDescriptionsText(Base):
	__tablename__ = 'generals_descriptions_texts'
	
	generals_descriptions_text_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	generals_description_id = Column( Integer, ForeignKey("generals_descriptions.generals_description_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	generals_descriptions_text_string = Column( String )
	generals_descriptions_text_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	generalsDescription = relationship( "GeneralsDescription", foreign_keys=[generals_description_id], backref="generalsDescriptionsTexts" )

	def __init__(self
		, generals_description_id_
		, generals_descriptions_text_string_
		, generals_descriptions_text_lang_
		, created_at_
		, updated_at_
 ):

		self.generals_description_id = generals_description_id_
		self.generals_descriptions_text_string = generals_descriptions_text_string_
		self.generals_descriptions_text_lang = generals_descriptions_text_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<GeneralsDescriptionsText %(generals_description_id)s %(generals_descriptions_text_id)s>' % self.__dict__

class GeneralsKeywordsText(Base):
	__tablename__ = 'generals_keywords_texts'
	
	generals_keywords_text_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	generals_keyword_id = Column( Integer, ForeignKey("generals_keywords.generals_keyword_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	generals_keywords_text_string = Column( String )
	generals_keywords_text_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	generalsKeyword = relationship( "GeneralsKeyword", foreign_keys=[generals_keyword_id], backref="generalsKeywordsTexts" )

	def __init__(self
		, generals_keyword_id_
		, generals_keywords_text_string_
		, generals_keywords_text_lang_
		, created_at_
		, updated_at_
 ):

		self.generals_keyword_id = generals_keyword_id_
		self.generals_keywords_text_string = generals_keywords_text_string_
		self.generals_keywords_text_lang = generals_keywords_text_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<GeneralsKeywordsText %(generals_keyword_id)s %(generals_keywords_text_id)s>' % self.__dict__

class GeneralsCoverage(Base):
	__tablename__ = 'generals_coverages'
	
	generals_coverage_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	general_id = Column( Integer, ForeignKey("generals.general_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	general = relationship( "General", foreign_keys=[general_id], backref="generalsCoverages" )

	def __init__(self
		, general_id_
		, created_at_
		, updated_at_
 ):

		self.general_id = general_id_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<GeneralsCoverage %(general_id)s %(generals_coverage_id)s>' % self.__dict__

class MetametadatasContributesEntity(Base):
	__tablename__ = 'metametadatas_contributes_entitys'
	
	contributes_entity_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	contribute_id = Column( Integer, ForeignKey("metametadatas_contributes.metametadatas_contribute_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	contributes_entity_string = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	metametadatasContribute = relationship( "MetametadatasContribute", foreign_keys=[contribute_id], backref="metametadatasContributesEntitys" )

	def __init__(self
		, contribute_id_
		, contributes_entity_string_
		, created_at_
		, updated_at_
 ):

		self.contribute_id = contribute_id_
		self.contributes_entity_string = contributes_entity_string_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<MetametadatasContributesEntity %(contributes_entity_id)s %(contribute_id)s>' % self.__dict__

class TechnicalsFormat(Base):
	__tablename__ = 'technicals_formats'
	
	technicals_format_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	technical_id = Column( Integer, ForeignKey("technicals.technical_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	technicals_format_text = Column( String(500) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	technical = relationship( "Technical", foreign_keys=[technical_id], backref="technicalsFormats" )

	def __init__(self
		, technical_id_
		, technicals_format_text_
		, created_at_
		, updated_at_
 ):

		self.technical_id = technical_id_
		self.technicals_format_text = technicals_format_text_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<TechnicalsFormat %(technicals_format_id)s %(technical_id)s>' % self.__dict__

class TechnicalsLocation(Base):
	__tablename__ = 'technicals_locations'
	
	technicals_location_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	technical_id = Column( Integer, ForeignKey("technicals.technical_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	technicals_location_text = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	technical = relationship( "Technical", foreign_keys=[technical_id], backref="technicalsLocations" )

	def __init__(self
		, technical_id_
		, technicals_location_text_
		, created_at_
		, updated_at_
 ):

		self.technical_id = technical_id_
		self.technicals_location_text = technicals_location_text_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<TechnicalsLocation %(technicals_location_id)s %(technical_id)s>' % self.__dict__

class TechnicalsInstallationremark(Base):
	__tablename__ = 'technicals_installationremarks'
	
	technicals_installationremark_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	technical_id = Column( Integer, ForeignKey("technicals.technical_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	technicals_installationremark_string = Column( String )
	technicals_installationremark_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	technical = relationship( "Technical", foreign_keys=[technical_id], backref="technicalsInstallationremarks" )

	def __init__(self
		, technical_id_
		, technicals_installationremark_string_
		, technicals_installationremark_lang_
		, created_at_
		, updated_at_
 ):

		self.technical_id = technical_id_
		self.technicals_installationremark_string = technicals_installationremark_string_
		self.technicals_installationremark_lang = technicals_installationremark_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<TechnicalsInstallationremark %(technicals_installationremark_id)s %(technical_id)s>' % self.__dict__

class TechnicalsOtherplatformrequirement(Base):
	__tablename__ = 'technicals_otherplatformrequirements'
	
	technicals_otherplatformrequirement_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	technical_id = Column( Integer, ForeignKey("technicals.technical_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	technicals_otherplatformrequirement_string = Column( String )
	technicals_otherplatformrequirement_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	technical = relationship( "Technical", foreign_keys=[technical_id], backref="technicalsOtherplatformrequirements" )

	def __init__(self
		, technical_id_
		, technicals_otherplatformrequirement_string_
		, technicals_otherplatformrequirement_lang_
		, created_at_
		, updated_at_
 ):

		self.technical_id = technical_id_
		self.technicals_otherplatformrequirement_string = technicals_otherplatformrequirement_string_
		self.technicals_otherplatformrequirement_lang = technicals_otherplatformrequirement_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<TechnicalsOtherplatformrequirement %(technicals_otherplatformrequirement_id)s %(technical_id)s>' % self.__dict__

class ClassificationsKeyword(Base):
	__tablename__ = 'classifications_keywords'
	
	classifications_keyword_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	classification_id = Column( Integer, ForeignKey("classifications.classification_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	classification = relationship( "Classification", foreign_keys=[classification_id], backref="classificationsKeywords" )

	def __init__(self
		, classification_id_
		, created_at_
		, updated_at_
 ):

		self.classification_id = classification_id_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<ClassificationsKeyword %(classification_id)s %(classifications_keyword_id)s>' % self.__dict__

class EducationalsLearningresourcetype(Base):
	__tablename__ = 'educationals_learningresourcetypes'
	
	educationals_learningresourcetype_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	educational_id = Column( Integer, ForeignKey("educationals.educational_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	educationals_learningresourcetype_string = Column( String(1000) )
	educationals_learningresourcetype_source = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	educational = relationship( "Educational", foreign_keys=[educational_id], backref="educationalsLearningresourcetypes" )

	def __init__(self
		, educational_id_
		, educationals_learningresourcetype_string_
		, educationals_learningresourcetype_source_
		, created_at_
		, updated_at_
 ):

		self.educational_id = educational_id_
		self.educationals_learningresourcetype_string = educationals_learningresourcetype_string_
		self.educationals_learningresourcetype_source = educationals_learningresourcetype_source_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<EducationalsLearningresourcetype %(educational_id)s %(educationals_learningresourcetype_id)s>' % self.__dict__

class EducationalsUserrole(Base):
	__tablename__ = 'educationals_userroles'
	
	educationals_userrole_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	educational_id = Column( Integer, ForeignKey("educationals.educational_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	educationals_userrole_string = Column( String(1000) )
	educationals_userrole_source = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	educational = relationship( "Educational", foreign_keys=[educational_id], backref="educationalsUserroles" )

	def __init__(self
		, educational_id_
		, educationals_userrole_string_
		, educationals_userrole_source_
		, created_at_
		, updated_at_
 ):

		self.educational_id = educational_id_
		self.educationals_userrole_string = educationals_userrole_string_
		self.educationals_userrole_source = educationals_userrole_source_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<EducationalsUserrole %(educational_id)s %(educationals_userrole_id)s>' % self.__dict__

class EducationalsContext(Base):
	__tablename__ = 'educationals_contexts'
	
	educationals_context_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	educational_id = Column( Integer, ForeignKey("educationals.educational_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	educationals_context_string = Column( String(1000) )
	educationals_context_source = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	educational = relationship( "Educational", foreign_keys=[educational_id], backref="educationalsContexts" )

	def __init__(self
		, educational_id_
		, educationals_context_string_
		, educationals_context_source_
		, created_at_
		, updated_at_
 ):

		self.educational_id = educational_id_
		self.educationals_context_string = educationals_context_string_
		self.educationals_context_source = educationals_context_source_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<EducationalsContext %(educational_id)s %(educationals_context_id)s>' % self.__dict__

class EducationalsTypicalagerange(Base):
	__tablename__ = 'educationals_typicalageranges'
	
	educationals_typicalagerange_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	educational_id = Column( Integer, ForeignKey("educationals.educational_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	educational = relationship( "Educational", foreign_keys=[educational_id], backref="educationalsTypicalageranges" )

	def __init__(self
		, educational_id_
		, created_at_
		, updated_at_
 ):

		self.educational_id = educational_id_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<EducationalsTypicalagerange %(educationals_typicalagerange_id)s %(educational_id)s>' % self.__dict__

class EducationalsDescription(Base):
	__tablename__ = 'educationals_descriptions'
	
	educationals_description_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	educational_id = Column( Integer, ForeignKey("educationals.educational_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	educational = relationship( "Educational", foreign_keys=[educational_id], backref="educationalsDescriptions" )

	def __init__(self
		, educational_id_
		, created_at_
		, updated_at_
 ):

		self.educational_id = educational_id_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<EducationalsDescription %(educational_id)s %(educationals_description_id)s>' % self.__dict__

class EducationalsLanguage(Base):
	__tablename__ = 'educationals_languages'
	
	educationals_language_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	educational_id = Column( Integer, ForeignKey("educationals.educational_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	educationals_language_string = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	educational = relationship( "Educational", foreign_keys=[educational_id], backref="educationalsLanguages" )

	def __init__(self
		, educational_id_
		, educationals_language_string_
		, created_at_
		, updated_at_
 ):

		self.educational_id = educational_id_
		self.educationals_language_string = educationals_language_string_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<EducationalsLanguage %(educationals_language_id)s %(educational_id)s>' % self.__dict__

class GeneralsTitle(Base):
	__tablename__ = 'generals_titles'
	
	generals_title_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	general_id = Column( Integer, ForeignKey("generals.general_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	generals_title_string = Column( String )
	generals_title_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	general = relationship( "General", foreign_keys=[general_id], backref="generalsTitles" )

	def __init__(self
		, general_id_
		, generals_title_string_
		, generals_title_lang_
		, created_at_
		, updated_at_
 ):

		self.general_id = general_id_
		self.generals_title_string = generals_title_string_
		self.generals_title_lang = generals_title_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<GeneralsTitle %(generals_title_id)s %(general_id)s>' % self.__dict__

class GeneralsKeyword(Base):
	__tablename__ = 'generals_keywords'
	
	generals_keyword_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	general_id = Column( Integer, ForeignKey("generals.general_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	general = relationship( "General", foreign_keys=[general_id], backref="generalsKeywords" )

	def __init__(self
		, general_id_
		, created_at_
		, updated_at_
 ):

		self.general_id = general_id_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<GeneralsKeyword %(general_id)s %(generals_keyword_id)s>' % self.__dict__

class GeneralsCoveragesText(Base):
	__tablename__ = 'generals_coverages_texts'
	
	generals_coverages_text_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	generals_coverage_id = Column( Integer, ForeignKey("generals_coverages.generals_coverage_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	generals_coverages_text_string = Column( String )
	generals_coverages_text_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	generalsCoverage = relationship( "GeneralsCoverage", foreign_keys=[generals_coverage_id], backref="generalsCoveragesTexts" )

	def __init__(self
		, generals_coverage_id_
		, generals_coverages_text_string_
		, generals_coverages_text_lang_
		, created_at_
		, updated_at_
 ):

		self.generals_coverage_id = generals_coverage_id_
		self.generals_coverages_text_string = generals_coverages_text_string_
		self.generals_coverages_text_lang = generals_coverages_text_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<GeneralsCoveragesText %(generals_coverage_id)s %(generals_coverages_text_id)s>' % self.__dict__

class MetametadatasSchema(Base):
	__tablename__ = 'metametadatas_schemas'
	
	metametadatas_schema_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	metametadata_id = Column( Integer, ForeignKey("metametadatas.metametadata_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	metametadatas_schema_text = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	metametadata = relationship( "Metametadata", foreign_keys=[metametadata_id], backref="metametadatasSchemas" )

	def __init__(self
		, metametadata_id_
		, metametadatas_schema_text_
		, created_at_
		, updated_at_
 ):

		self.metametadata_id = metametadata_id_
		self.metametadatas_schema_text = metametadatas_schema_text_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<MetametadatasSchema %(metametadatas_schema_id)s %(metametadata_id)s>' % self.__dict__

class GeneralsDescription(Base):
	__tablename__ = 'generals_descriptions'
	
	generals_description_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	general_id = Column( Integer, ForeignKey("generals.general_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	general = relationship( "General", foreign_keys=[general_id], backref="generalsDescriptions" )

	def __init__(self
		, general_id_
		, created_at_
		, updated_at_
 ):

		self.general_id = general_id_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<GeneralsDescription %(general_id)s %(generals_description_id)s>' % self.__dict__

class GeneralsIdentifier(Base):
	__tablename__ = 'generals_identifiers'
	
	generals_identifier_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	general_id = Column( Integer, ForeignKey("generals.general_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	generals_identifier_catalog = Column( String(1000) )
	generals_identifier_entry = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	general = relationship( "General", foreign_keys=[general_id], backref="generalsIdentifiers" )

	def __init__(self
		, general_id_
		, generals_identifier_catalog_
		, generals_identifier_entry_
		, created_at_
		, updated_at_
 ):

		self.general_id = general_id_
		self.generals_identifier_catalog = generals_identifier_catalog_
		self.generals_identifier_entry = generals_identifier_entry_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<GeneralsIdentifier %(generals_identifier_id)s %(general_id)s>' % self.__dict__

class ResourcesIdentifier(Base):
	__tablename__ = 'resources_identifiers'
	
	resources_identifier_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	resource_id = Column( Integer, ForeignKey("resources.resource_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	resources_identifier_catalog = Column( String(1000) )
	resources_identifier_entry = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	resource = relationship( "Resource", foreign_keys=[resource_id], backref="resourcesIdentifiers" )

	def __init__(self
		, resource_id_
		, resources_identifier_catalog_
		, resources_identifier_entry_
		, created_at_
		, updated_at_
 ):

		self.resource_id = resource_id_
		self.resources_identifier_catalog = resources_identifier_catalog_
		self.resources_identifier_entry = resources_identifier_entry_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<ResourcesIdentifier %(resource_id)s %(resources_identifier_id)s>' % self.__dict__

class ResourcesDescription(Base):
	__tablename__ = 'resources_descriptions'
	
	resources_description_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	resource_id = Column( Integer, ForeignKey("resources.resource_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	resource = relationship( "Resource", foreign_keys=[resource_id], backref="resourcesDescriptions" )

	def __init__(self
		, resource_id_
		, created_at_
		, updated_at_
 ):

		self.resource_id = resource_id_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<ResourcesDescription %(resources_description_id)s %(resource_id)s>' % self.__dict__

class ResourcesDescriptionsText(Base):
	__tablename__ = 'resources_descriptions_texts'
	
	resources_descriptions_text_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	resources_description_id = Column( Integer, ForeignKey("resources_descriptions.resources_description_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )
	resources_descriptions_text_string = Column( String )
	resources_descriptions_text_lang = Column( String(100) )

	resourcesDescription = relationship( "ResourcesDescription", foreign_keys=[resources_description_id], backref="resourcesDescriptionsTexts" )

	def __init__(self
		, resources_description_id_
		, resources_descriptions_text_string_
		, resources_descriptions_text_lang_
		, created_at_
		, updated_at_
		
 ):

		self.resources_description_id = resources_description_id_
		self.created_at = created_at_
		self.updated_at = updated_at_
		self.resources_descriptions_text_string = resources_descriptions_text_string_
		self.resources_descriptions_text_lang = resources_descriptions_text_lang_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<ResourcesDescriptionsText %(resources_descriptions_text_id)s %(resources_description_id)s>' % self.__dict__

class AnnotationsDescription(Base):
	__tablename__ = 'annotations_descriptions'
	
	annotations_description_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	annotation_id = Column( Integer, ForeignKey("annotations.annotation_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	annotations_description_string = Column( String )
	annotations_description_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	annotation = relationship( "Annotation", foreign_keys=[annotation_id], backref="annotationsDescriptions" )

	def __init__(self
		, annotation_id_
		, annotations_description_string_
		, annotations_description_lang_
		, created_at_
		, updated_at_
 ):

		self.annotation_id = annotation_id_
		self.annotations_description_string = annotations_description_string_
		self.annotations_description_lang = annotations_description_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<AnnotationsDescription %(annotations_description_id)s %(annotation_id)s>' % self.__dict__

class LifecyclesVersion(Base):
	__tablename__ = 'lifecycles_versions'
	
	lifecycles_version_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	lifecycle_id = Column( Integer, ForeignKey("lifecycles.lifecycle_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	lifecycles_version_string = Column( String )
	lifecycles_version_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	lifecycle = relationship( "Lifecycle", foreign_keys=[lifecycle_id], backref="lifecyclesVersions" )

	def __init__(self
		, lifecycle_id_
		, lifecycles_version_string_
		, lifecycles_version_lang_
		, created_at_
		, updated_at_
 ):

		self.lifecycle_id = lifecycle_id_
		self.lifecycles_version_string = lifecycles_version_string_
		self.lifecycles_version_lang = lifecycles_version_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<LifecyclesVersion %(lifecycle_id)s %(lifecycles_version_id)s>' % self.__dict__

class MetametadatasIdentifier(Base):
	__tablename__ = 'metametadatas_identifiers'
	
	metametadatas_identifier_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	metametadata_id = Column( Integer, ForeignKey("metametadatas.metametadata_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	metametadatas_identifier_catalog = Column( String(1000) )
	metametadatas_identifier_entry = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	metametadata = relationship( "Metametadata", foreign_keys=[metametadata_id], backref="metametadatasIdentifiers" )

	def __init__(self
		, metametadata_id_
		, metametadatas_identifier_catalog_
		, metametadatas_identifier_entry_
		, created_at_
		, updated_at_
 ):

		self.metametadata_id = metametadata_id_
		self.metametadatas_identifier_catalog = metametadatas_identifier_catalog_
		self.metametadatas_identifier_entry = metametadatas_identifier_entry_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<MetametadatasIdentifier %(metametadatas_identifier_id)s %(metametadata_id)s>' % self.__dict__

class LifecyclesContribute(Base):
	__tablename__ = 'lifecycles_contributes'
	
	lifecycles_contribute_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	lifecycle_id = Column( Integer, ForeignKey("lifecycles.lifecycle_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	lifecycles_contribute_role = Column( String(1000) )
	lifecycles_contribute_role_source = Column( String(1000) )
	lifecycles_contribute_date = Column( DateTime )
	lifecycles_contribute_date_description = Column( String )
	lifecycles_contribute_date_description_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	lifecycle = relationship( "Lifecycle", foreign_keys=[lifecycle_id], backref="lifecyclesContributes" )

	def __init__(self
		, lifecycle_id_
		, lifecycles_contribute_role_
		, lifecycles_contribute_role_source_
		, lifecycles_contribute_date_
		, lifecycles_contribute_date_description_
		, lifecycles_contribute_date_description_lang_
		, created_at_
		, updated_at_
 ):

		self.lifecycle_id = lifecycle_id_
		self.lifecycles_contribute_role = lifecycles_contribute_role_
		self.lifecycles_contribute_role_source = lifecycles_contribute_role_source_
		self.lifecycles_contribute_date = lifecycles_contribute_date_
		self.lifecycles_contribute_date_description = lifecycles_contribute_date_description_
		self.lifecycles_contribute_date_description_lang = lifecycles_contribute_date_description_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<LifecyclesContribute %(lifecycle_id)s %(lifecycles_contribute_id)s>' % self.__dict__

class LifecyclesContributesEntity(Base):
	__tablename__ = 'lifecycles_contributes_entitys'
	
	lifecycles_contributes_entity_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	lifecycles_contribute_id = Column( Integer, ForeignKey("lifecycles_contributes.lifecycles_contribute_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	lifecycles_contributes_entity_string = Column( String(1000) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	lifecyclesContribute = relationship( "LifecyclesContribute", foreign_keys=[lifecycles_contribute_id], backref="lifecyclesContributesEntitys" )

	def __init__(self
		, lifecycles_contribute_id_
		, lifecycles_contributes_entity_string_
		, created_at_
		, updated_at_
 ):

		self.lifecycles_contribute_id = lifecycles_contribute_id_
		self.lifecycles_contributes_entity_string = lifecycles_contributes_entity_string_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<LifecyclesContributesEntity %(lifecycles_contributes_entity_id)s %(lifecycles_contribute_id)s>' % self.__dict__

class ClassificationsKeywordsText(Base):
	__tablename__ = 'classifications_keywords_texts'
	
	classifications_keywords_text_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	classifications_keyword_id = Column( Integer, ForeignKey("classifications_keywords.classifications_keyword_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	classifications_keywords_text_string = Column( String )
	classifications_keywords_text_lang = Column( String(100) )
	updated_at = Column( DateTime )
	created_at = Column( DateTime )

	classificationsKeyword = relationship( "ClassificationsKeyword", foreign_keys=[classifications_keyword_id], backref="classificationsKeywordsTexts" )

	def __init__(self
		, classifications_keyword_id_
		, classifications_keywords_text_string_
		, classifications_keywords_text_lang_
		, updated_at_
		, created_at_
 ):

		self.classifications_keyword_id = classifications_keyword_id_
		self.classifications_keywords_text_string = classifications_keywords_text_string_
		self.classifications_keywords_text_lang = classifications_keywords_text_lang_
		self.updated_at = updated_at_
		self.created_at = created_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<ClassificationsKeywordsText %(classifications_keywords_text_id)s %(classifications_keyword_id)s>' % self.__dict__

class ClassificationsDescription(Base):
	__tablename__ = 'classifications_descriptions'
	
	classifications_description_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	classification_id = Column( Integer, ForeignKey("classifications.classification_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	classifications_description_string = Column( String )
	classifications_description_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	classification = relationship( "Classification", foreign_keys=[classification_id], backref="classificationsDescriptions" )

	def __init__(self
		, classification_id_
		, classifications_description_string_
		, classifications_description_lang_
		, created_at_
		, updated_at_
 ):

		self.classification_id = classification_id_
		self.classifications_description_string = classifications_description_string_
		self.classifications_description_lang = classifications_description_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<ClassificationsDescription %(classification_id)s %(classifications_description_id)s>' % self.__dict__

class TaxonpathsSource(Base):
	__tablename__ = 'taxonpaths_sources'
	
	taxonpaths_source_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	taxonpath_id = Column( Integer, ForeignKey("taxonpaths.taxonpath_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	taxonpaths_source_string = Column( String )
	taxonpaths_source_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	taxonpath = relationship( "Taxonpath", foreign_keys=[taxonpath_id], backref="taxonpathsSources" )

	def __init__(self
		, taxonpath_id_
		, taxonpaths_source_string_
		, taxonpaths_source_lang_
		, created_at_
		, updated_at_
 ):

		self.taxonpath_id = taxonpath_id_
		self.taxonpaths_source_string = taxonpaths_source_string_
		self.taxonpaths_source_lang = taxonpaths_source_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<TaxonpathsSource %(taxonpaths_source_id)s %(taxonpath_id)s>' % self.__dict__

class TaxonsEntry(Base):
	__tablename__ = 'taxons_entrys'
	
	taxons_entry_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	taxon_id = Column( Integer, ForeignKey("taxons.taxon_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	taxons_entry_string = Column( String )
	taxons_entry_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	taxon = relationship( "Taxon", foreign_keys=[taxon_id], backref="taxonsEntrys" )

	def __init__(self
		, taxon_id_
		, taxons_entry_string_
		, taxons_entry_lang_
		, created_at_
		, updated_at_
 ):

		self.taxon_id = taxon_id_
		self.taxons_entry_string = taxons_entry_string_
		self.taxons_entry_lang = taxons_entry_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<TaxonsEntry %(taxons_entry_id)s %(taxon_id)s>' % self.__dict__

class RightsDescription(Base):
	__tablename__ = 'rights_descriptions'
	
	rights_description_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	right_id = Column( Integer, ForeignKey("rights.right_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	rights_description_string = Column( String )
	rights_description_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	right = relationship( "Right", foreign_keys=[right_id], backref="rightsDescriptions" )

	def __init__(self
		, right_id_
		, rights_description_string_
		, rights_description_lang_
		, created_at_
		, updated_at_
 ):

		self.right_id = right_id_
		self.rights_description_string = rights_description_string_
		self.rights_description_lang = rights_description_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<RightsDescription %(right_id)s %(rights_description_id)s>' % self.__dict__

class EducationalsDescriptionsText(Base):
	__tablename__ = 'educationals_descriptions_texts'
	
	educationals_descriptions_text_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	educationals_description_id = Column( Integer, ForeignKey("educationals_descriptions.educationals_description_id"), nullable=False, primary_key=True, autoincrement=False, index=True )
	educationals_descriptions_text_string = Column( String )
	educationals_descriptions_text_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )

	educationalsDescription = relationship( "EducationalsDescription", foreign_keys=[educationals_description_id], backref="educationalsDescriptionsTexts" )

	def __init__(self
		, educationals_description_id_
		, educationals_descriptions_text_string_
		, educationals_descriptions_text_lang_
		, created_at_
		, updated_at_
 ):

		self.educationals_description_id = educationals_description_id_
		self.educationals_descriptions_text_string = educationals_descriptions_text_string_
		self.educationals_descriptions_text_lang = educationals_descriptions_text_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<EducationalsDescriptionsText %(educationals_descriptions_text_id)s %(educationals_description_id)s>' % self.__dict__

class EducationalsTypicalagerangesText(Base):
	__tablename__ = 'educationals_typicalageranges_texts'
	
	educationals_typicalageranges_text_id = Column( Integer, nullable=False, autoincrement=True, primary_key=True, unique=True )
	educationals_typicalagerange_id = Column( Integer, nullable=False, primary_key=True, autoincrement=False )
	educationals_typicalageranges_text_string = Column( String )
	educationals_typicalageranges_text_lang = Column( String(100) )
	created_at = Column( DateTime )
	updated_at = Column( DateTime )


	def __init__(self
		, educationals_typicalagerange_id_
		, educationals_typicalageranges_text_string_
		, educationals_typicalageranges_text_lang_
		, created_at_
		, updated_at_
 ):

		self.educationals_typicalagerange_id = educationals_typicalagerange_id_
		self.educationals_typicalageranges_text_string = educationals_typicalageranges_text_string_
		self.educationals_typicalageranges_text_lang = educationals_typicalageranges_text_lang_
		self.created_at = created_at_
		self.updated_at = updated_at_

	def __repr__( self ):
		return self.__str__()

	def __str__( self ):
		return '<EducationalsTypicalagerangesText %(educationals_typicalagerange_id)s %(educationals_typicalageranges_text_id)s>' % self.__dict__
