'''

	Paco Gallardo 2013

'''

from conf import Config
from models import General, GeneralsCoveragesText, GeneralsKeyword, \
	GeneralsDescriptionsText, GeneralsLanguage, GeneralsIdentifier, Lifecycle, \
	LifecyclesVersion, LifecyclesContributesEntity, Metametadata, \
	MetametadatasSchema, Relation, Resource, ResourcesDescription, \
	ResourcesDescriptionsText, ResourcesIdentifier, Technical, \
	TechnicalsInstallationremark, TechnicalsOtherplatformrequirement, \
	TechnicalsLocation, TechnicalsFormat, Requirement, Orcomposite, Educational, \
	EducationalsDescription, EducationalsDescriptionsText, EducationalsLanguage, \
	EducationalsTypicalagerange, EducationalsTypicalagerangesText, \
	EducationalsUserrole, EducationalsLearningresourcetype, Right, RightsDescription, \
	Annotation, Classification, ClassificationsKeyword, ClassificationsKeywordsText, \
	ClassificationsDescription, Taxonpath, TaxonpathsSource, Taxon, TaxonsEntry, Lom, \
	AnnotationsDescription, EducationalsContext, MetametadatasContributesEntity, \
	MetametadatasIdentifier, MetametadatasContribute, LifecyclesContribute, \
	GeneralsTitle, GeneralsDescription, GeneralsKeywordsText, GeneralsCoverage
from sqlalchemy.engine import create_engine
from sqlalchemy.orm.session import sessionmaker
import dateutil.parser
from sqlalchemy.exc import InterfaceError
import time

class Controller :
	
	_session = ''
	_config = ''

	def __init__(self) :
		try : 
			self._config = Config()
			self.__createSession()
		except InterfaceError , err:
			print 'ERROR : %s' % err

	def __createSession(self) : 
		try :
			some_engine = create_engine(self._config.engine)
			Session = sessionmaker(bind=some_engine)
			self._session = Session()
		except InterfaceError :
			print ('Error: can not connect to Database')

	def handlerLom(self):
		obj = Lom(self.__now(), self.__now())
		self.commit(obj)
		return obj.id

	# ## Generals
	
	def handlerGenerals(self, general, lom_id) :
		general_structure = ''
		general_structure_source = ''
		general_aggregation_level = ''
		general_aggregation_level_source = ''
		for item in general.childNodes :
			if self.is_nodeElement(item) :
				if item.tagName == 'structure':
					general_structure = item.getElementsByTagName('value')[0].firstChild.nodeValue
					general_structure_source = item.getElementsByTagName('source')[0].firstChild.nodeValue
				if item.tagName == 'aggregationLevel':
					general_aggregation_level = item.getElementsByTagName('value')[0].firstChild.nodeValue
					general_aggregation_level_source = item.getElementsByTagName('source')[0].firstChild.nodeValue
		coverages = general.getElementsByTagName('converage')
		keywords = general.getElementsByTagName('keyword')
		titles = general.getElementsByTagName('title')
		identifiers = general.getElementsByTagName('identifier')
		languages = general.getElementsByTagName('language')
		descriptions = general.getElementsByTagName('description')
		obj = General(lom_id
 	    	, general_structure
 	    	, general_structure_source
 	    	, general_aggregation_level
 	    	, general_aggregation_level_source
 	    	, self.__now()
 	    	, self.__now())
		self.commit(obj)

	 	self.__handlerGeneralsCoverages(coverages, obj.general_id)
	 	self.__handlerGeneralsKeywords(keywords, obj.general_id) 
		self.__handlerGeneralsTitles(titles, obj.general_id)
		self.__handlerGeneralsIdentifiers(identifiers, obj.general_id)
		self.__handlerGeneralsLanguages(languages, obj.general_id)
		self.__handlerGeneralsDescriptions(descriptions, obj.general_id)
 	
	def __handlerGeneralsCoverages(self, coverages, general_id) :
		for item in coverages:
			obj = GeneralsCoverage(general_id, self.__now(), self.__now())
			self.commit(obj)
			self.__handlerGeneralsCoverage(item, obj.generals_converage_text_id)
	
	def __handlerGeneralsCoverage(self, nodeO, generals_converage_text_id) :
		for item in nodeO.childNodes :
			if self.is_nodeElement(item):
				try:
					generals_coverages_text_string = item.firstChild.nodeValue
					generals_coverages_text_lang = ''
					if item.hasAttribute('language') :
						generals_coverages_text_lang = item.attributes['language'].value
					obj = GeneralsCoveragesText(generals_converage_text_id
				    	, generals_coverages_text_string
				    	, generals_coverages_text_lang
				    	, self.__now()
				    	, self.__now())
					self.commit(obj)
				except AttributeError:
					pass
				
			
	def __handlerGeneralsKeywords(self, nodeO, general_id) :
		for item in nodeO :
			obj = GeneralsKeyword(general_id, self.__now(), self.__now())
			self.commit(obj)
			self.__handlerGeneralKeyword(item, obj.generals_keyword_id)
	
	def __handlerGeneralKeyword(self, nodeO, generals_keyword_id) :
		for item in nodeO.childNodes:
			if self.is_nodeElement(item) and item.tagName == 'string' :
				try:
					generals_keywords_text_string = item.firstChild.nodeValue
					generals_keywords_text_lang = ''
					if item.hasAttribute('language') :
						generals_keywords_text_lang = item.attributes['language'].value
					obj = GeneralsKeywordsText(generals_keyword_id, generals_keywords_text_string, generals_keywords_text_lang, self.__now(), self.__now())
					self.commit(obj)
				except AttributeError:
					pass
				
	
	def __handlerGeneralsTitles(self, nodeO, general_id) :
		for items in nodeO :
			for item in items.childNodes :
				if self.is_nodeElement(item) and item.tagName == 'string':
					try:
						generals_title_string = item.firstChild.nodeValue
						generals_title_lang = ''
						if item.hasAttribute('language') :
							generals_title_lang = item.attributes['language'].value
						obj = GeneralsTitle(general_id, generals_title_string, generals_title_lang, self.__now(), self.__now())
						self.commit(obj)
					except AttributeError:
						pass
	
	def __handlerGeneralsDescriptions(self, nodeO, general_id) :
		for item in nodeO :
			obj = GeneralsDescription(general_id, self.__now(), self.__now())
			self.commit(obj)
			self.__handlerGeneralsDescription(item, obj.generals_description_id)
			
	def __handlerGeneralsDescription(self, nodeO, generals_description_id) :
		for item in nodeO.childNodes:
			if self.is_nodeElement(item) and item.tagName == 'string':
				try:
					generals_descriptions_text_string = item.firstChild.nodeValue
					generals_descriptions_text_lang = ''
					if item.hasAttribute('language') :
						generals_descriptions_text_lang = item.attributes['language'].value
					obj = GeneralsDescriptionsText(generals_description_id, generals_descriptions_text_string, generals_descriptions_text_lang, self.__now(), self.__now())
					self.commit(obj)
				except AttributeError:
					pass
				
	
	def __handlerGeneralsLanguages(self, nodeO, general_id) :
		for item in nodeO:
			language = item.firstChild.nodeValue
			obj = GeneralsLanguage(general_id, language, self.__now(), self.__now())
			self.commit(obj)
	
	def __handlerGeneralsIdentifiers(self, nodeO, general_id) :
		for item in nodeO:
			catalog = ''
			entry = ''
			for item1 in item.childNodes :
				if self.is_nodeElement(item1) : 
					if item1.tagName == 'catalog' :
						catalog = item1.firstChild.nodeValue
					if item1.tagName == 'entry' :
						entry = item1.firstChild.nodeValue
			obj = GeneralsIdentifier(general_id, catalog, entry, self.__now(), self.__now())
			self.commit(obj)
				
	# ## lifeCycle
	
	def handlerLifecycle(self, nodeO, lom_id) :
		lifecycle_status = ''
		lifecycle_status_source = ''
		status = nodeO.getElementsByTagName('status')
		for items in status :
			for item in items.childNodes :
				if self.is_nodeElement(item):
					if item.tagName == 'value' : 
						lifecycle_status = item.firstChild.nodeValue
					if item.tagName == 'source' :
						try:
							lifecycle_status_source = item.firstChild.nodeValue
						except AttributeError:
							lifecycle_status_source = ''
		
		versions = nodeO.getElementsByTagName('version')
		contributes = nodeO.getElementsByTagName('contribute')
		
		obj = Lifecycle(lom_id
			, lifecycle_status
			, lifecycle_status_source
			, self.__now()
			, self.__now())
		self.commit(obj)
	
		if not versions == '' :
			self.__handlerLifecyclesVersions(versions, obj.lifecycle_id)
		
		if not contributes == '' :
			self.__handlerLifecyclesContributes(contributes, obj.lifecycle_id)
	
	
	def __handlerLifecyclesVersions(self, nodeO, lifecycle_id) : 
		for items in nodeO :
			for item in items.childNodes : 
				if self.is_nodeElement(item) :
					try:
						nodeO_string = item.firstChild.nodeValue
						nodeO_lang = ''
						if item.hasAttribute('language') :
							nodeO_lang = item.attributes['language'].value
						obj = LifecyclesVersion(lifecycle_id, nodeO_string, nodeO_lang, self.__now(), self.__now())
						self.commit(obj)
					except AttributeError:
						pass
					
	
	def __handlerLifecyclesContributes(self, nodeO, lifecycle_id) : 
		lifecycles_contribute_role = ''
		lifecycles_contribute_role_source = ''
		lifecycles_contribute_date = '9999-12-31 23:59:59'
		lifecycles_contribute_date_description = ''
		lifecycles_contribute_date_description_lang = ''

		for item in nodeO :
			role = item.getElementsByTagName('role')
			date = item.getElementsByTagName('date')
			entities = item.getElementsByTagName('entity')
		
			for items in role :
				for item in items.childNodes :
					if self.is_nodeElement(item):
						if item.tagName == 'value' : 
							lifecycles_contribute_role = item.firstChild.nodeValue
						if item.tagName == 'source' :
							try:
								lifecycles_contribute_role_source = item.firstChild.nodeValue
							except AttributeError:
								lifecycles_contribute_role_source = ''
			
			for items in date :
				for item in items.childNodes :
					if self.is_nodeElement(item) and item.tagName == 'dateTime' :
						lifecycles_contribute_date = dateutil.parser.parse(item.firstChild.nodeValue) 
			
		
			obj = LifecyclesContribute(lifecycle_id, lifecycles_contribute_role, lifecycles_contribute_role_source, lifecycles_contribute_date , lifecycles_contribute_date_description , lifecycles_contribute_date_description_lang, self.__now(), self.__now())
			self.commit(obj)
			self.__handlerLifecyclesContributesEntities(entities, obj.lifecycles_contribute_id)		
	
	def __handlerLifecyclesContributesEntities(self, nodeO, lifecycles_contribute_id) :
		for items in nodeO:
			for item in items.childNodes : 
				lifecycles_contributes_entity_string = item.data
				obj = LifecyclesContributesEntity(lifecycles_contribute_id
					, lifecycles_contributes_entity_string
					, self.__now()
					, self.__now())
				self.commit(obj)
	
	# ## metametadatas
	
	def handlerMetametadatas(self, metametadata, lom_id) :
		languages = metametadata.getElementsByTagName('language')
		metametadata_lang = ''
		for item in languages :
			metametadata_lang =	item.firstChild.nodeValue
		obj = Metametadata(lom_id
			, metametadata_lang
			, self.__now()
			, self.__now())
		self.commit(obj)
			
		identifiers = metametadata.getElementsByTagName('identifier')
		contributes = metametadata.getElementsByTagName('contribute')
		schemas = metametadata.getElementsByTagName('metadataSchema')
		
		self.__handlerMetametadatasIdentifiers(identifiers, obj.metametadata_id)
		self.__handlerMetametadatasContributes(contributes, obj.metametadata_id) 
		self.__handlerMetametadatasSchemas(schemas, obj.metametadata_id)
		
	
	def __handlerMetametadatasContributes(self, nodeO, metametadata_id) : 
		for items in nodeO :
			contribute_role = ''
			contribute_role_source = ''
			contribute_date = '9999-12-31 23:59:59'
			contribute_date_description = ''
			contribute_date_description_lang = ''
				
			for item in nodeO :
				role = item.getElementsByTagName('role')
				date = item.getElementsByTagName('date')
				entities = item.getElementsByTagName('entity')
		
				for items in role :
					for item in items.childNodes :
						if self.is_nodeElement(item):
							if item.tagName == 'value' : 
								contribute_role = item.firstChild.nodeValue
							if item.tagName == 'source' :
								try:
									contribute_role_source = item.firstChild.nodeValue
								except AttributeError:
									contribute_role_source = ''
				
				for items in date :
					for item in items.childNodes :
						if self.is_nodeElement(item) :
							if  item.tagName == 'dateTime' :
								contribute_date = dateutil.parser.parse(item.firstChild.nodeValue)
							if  item.tagName == 'description' :
								for item0 in item.childNodes :
									if self.is_nodeElement(item0):
										contribute_date_description = item0.firstChild.nodeValue
				
				obj = MetametadatasContribute(metametadata_id
					, contribute_role
			    	, contribute_role_source
			    	, contribute_date
			    	, contribute_date_description
			    	, contribute_date_description_lang
			    	, self.__now()
			    	, self.__now())
				self.commit(obj)
				self.__handlerMetametadatasContributesEntities(entities, obj.metametadatas_contribute_id)    
	
	def __handlerMetametadatasContributesEntities(self, nodeO, contribute_id) :    
		for items in nodeO:
			for item in items.childNodes : 
				contributes_entity_string = item.data
				obj = MetametadatasContributesEntity(contribute_id
					, contributes_entity_string
					, self.__now()
					, self.__now())
				self._session.add(obj)
		self._session.commit()
			
	def __handlerMetametadatasSchemas(self, nodeO, metametadata_id) :
		for item in nodeO :
			metametadatas_schema_text = item.firstChild.nodeValue
			obj = MetametadatasSchema(metametadata_id, metametadatas_schema_text, self.__now(), self.__now())
			self.commit(obj)
	
	def __handlerMetametadatasIdentifiers(self, nodeO, metametadata_id) :
		metametadatas_identifier_catalog = ''
		metametadatas_identifier_entry =  ''
		for items in nodeO :
			for item in items.childNodes :
				if self.is_nodeElement(item):
					if item.tagName == 'catalog':
						metametadatas_identifier_catalog = item.firstChild.nodeValue
					if item.tagName == 'entry':
						metametadatas_identifier_entry = item.firstChild.nodeValue
				obj = MetametadatasIdentifier(metametadata_id
			 		, metametadatas_identifier_catalog
			 		, metametadatas_identifier_entry
			 		, self.__now()
			 		, self.__now())
			self._session.add(obj)
		self._session.commit()
		
	# ## relations
	
	def handlerRelations(self, nodeO, lom_id) : 
		relation_kind = ''
		relation_kind_source = ''
		kind = nodeO.getElementsByTagName('kind')
		if len(kind) > 0 :
			for items in kind:
				for item in items.childNodes :
					if self.is_nodeElement(item):
						if item.tagName == 'value' :
							relation_kind = item.firstChild.nodeValue
						if item.tagName == 'source' :
							try:
								relation_kind_source = item.firstChild.nodeValue
							except AttributeError:
								relation_kind_source = ''
		
		obj = Relation(lom_id
			, relation_kind
			, relation_kind_source
			, self.__now()
			, self.__now())
		self.commit(obj)
		resources = nodeO.getElementsByTagName('resource')
		self.__handlerResources(resources, obj.relation_id)
	
	def __handlerResources(self, nodeO, relation_id) :
		for item in nodeO :
			obj = Resource(relation_id, self.__now(), self.__now())
			self.commit(obj)
	
			identifiers = item.getElementsByTagName('identifier')
			descriptions = item.getElementsByTagName('description')
			
			self.__handlerResourcesIdentifiers(identifiers, obj.resource_id)
			self.__handlerResourcesDescriptions(descriptions, obj.resource_id)
			
	def __handlerResourcesDescriptions(self, nodeO, resource_id) :
		for item in nodeO: 
			obj = ResourcesDescription(resource_id
				, self.__now()
				, self.__now())
			self.commit(obj)
			self.__handlerResourcesDescriptionsText(item, obj.resources_description_id)
	
	def __handlerResourcesDescriptionsText(self, nodeO, resources_description_id) :
		for item in nodeO.childNodes :
			nodeO_string = ''
			nodeO_lang = ''
			if self.is_nodeElement(item) and item.tagName == 'string':
				try:
					nodeO_string = item.firstChild.nodeValue
					nodeO_lang = ''
					if item.hasAttribute('language') :
						nodeO_lang = item.attributes['language'].value
					obj = ResourcesDescriptionsText(resources_description_id
						, nodeO_string
						, nodeO_lang
						, self.__now()
						, self.__now())
					self.commit(obj)
				except AttributeError:
					pass
	
	def __handlerResourcesIdentifiers(self, nodeO, resource_id):
		for items in nodeO :
			for item in items.childNodes :
				resources_identifier_catalog = ''
				resources_identifier_entry = ''
				if self.is_nodeElement(item):
					if item.tagName == 'catalog' :
						resources_identifier_catalog = item.firstChild.nodeValue
					if item.tagName == 'entry' :
						resources_identifier_entry = item.firstChild.nodeValue
			
				obj = ResourcesIdentifier(resource_id
					, resources_identifier_catalog
					, resources_identifier_entry
					, self.__now()
					, self.__now())
			self._session.add(obj)
		self._session.commit()
	
	# ## technicals
	
	def handlerTechnicals(self, nodeO, lom_id) :
		technical_duration_description = ''
		technical_duration_description_lang = ''
		technical_duration = ''
		technical_size = ''
		
		duration = nodeO.getElementsByTagName('duration')
		size = nodeO.getElementsByTagName('size')

		if len(duration) > 0 :
			for item in duration.childNodes :
				if self.is_nodeElement(item) and item.tagName == 'duration':
					technical_duration = item.firstChild.nodeValue
			
		if len(size) > 0 :
			for items in size :
				for item in items.childNodes :
					technical_size = item.data
			
		obj = Technical(lom_id
			, technical_size
			, technical_duration
			, technical_duration_description
			, technical_duration_description_lang
			, self.__now()
			, self.__now())
		self.commit(obj)
	
		formats = nodeO.getElementsByTagName('format')
		self.__handlerTechnicalsFormats(formats, obj.technical_id)
	
		locations = nodeO.getElementsByTagName('location')
		self.__handlerTechnicalsLocations(locations, obj.technical_id)
	
		installationRemarks = nodeO.getElementsByTagName('installationRemarks')
		self.__handlerTechnicalsInstallationremarks(installationRemarks, obj.technical_id)
	
		otherPlatformRequirements = nodeO.getElementsByTagName('otherPlatformRequirements')
		self.__handlerTechnicalsOtherplatformrequirements(otherPlatformRequirements, obj.technical_id)
		
		requirements = nodeO.getElementsByTagName('requirement')
		self.__handlerRequirements(requirements, obj.technical_id)
	
	def __handlerTechnicalsInstallationremarks(self, nodeO, technical_id):
		for items in nodeO :
			for item in items.childNodes :
				nodeO_string = ''
				nodeO_lang = ''
				if self.is_nodeElement(item) and item.tagName == 'string':
					try:
						nodeO_string = item.firstChild.nodeValue
						nodeO_lang = ''
						if item.hasAttribute('language') :
							nodeO_lang = item.attributes['language'].value	
						obj = TechnicalsInstallationremark(
							technical_id
							, nodeO_string
							, nodeO_lang
							, self.__now()
							, self.__now())
						self.commit(obj)
					except AttributeError:
						pass

	
	def __handlerTechnicalsOtherplatformrequirements(self, nodeO, technical_id):
		for items in nodeO :
			for item in items.childNodes :
				nodeO_string = ''
				nodeO_lang = ''
				if self.is_nodeElement(item) and item.tagName == 'string':
					try:
						nodeO_string = item.firstChild.nodeValue
						nodeO_lang = ''
						if item.hasAttribute('language') :
							nodeO_lang = item.attributes['language'].value
						obj = TechnicalsOtherplatformrequirement(
							technical_id
							, nodeO_string
							, nodeO_lang
							, self.__now()
							, self.__now())
						self.commit(obj)
					except AttributeError:
						nodeO_string = ''
		
				
	def __handlerTechnicalsLocations(self, nodeO, technical_id):
		for items in nodeO:
			for item in items.childNodes :
				technicals_location_text = item.data
				obj = TechnicalsLocation(
					technical_id
					, technicals_location_text
					, self.__now()
					, self.__now())
				self.commit(obj)
	
	def __handlerTechnicalsFormats(self, nodeO, technical_id):
		for items in nodeO:
			for item in items.childNodes :
				technicals_format_text = item.data
				obj = TechnicalsFormat(technical_id
					, technicals_format_text
					, self.__now()
					, self.__now())
				self.commit(obj)
	
	def __handlerRequirements(self, nodeO, technical_id):
		for item in nodeO:
			obj = Requirement(technical_id
				, self.__now()
				, self.__now())
			self.commit(obj)
			orcomposites = item.getElementsByTagName('orComposite') 
			self.__handlerOrComposite(orcomposites, obj.requirement_id)
	
	def __handlerOrComposite(self, nodeO, requirement_id) :
		for item in nodeO:
			orcomposite_type = ''
			orcomposite_type_source = ''
			orcomposite_name = ''
			orcomposite_name_source = ''
			orcomposite_minimumversion = ''
			orcomposite_maximumversion = ''						
			
			tComp = item.getElementsByTagName('type')
			for items1 in tComp :
				for item1 in items1.childNodes :
					if self.is_nodeElement(item1):
						if item1.tagName == 'value' :
							orcomposite_type = item1.firstChild.nodeValue
						if item1.tagName == 'source' :	
							try:
								orcomposite_type_source = item1.firstChild.nodeValue
							except AttributeError:
								orcomposite_type_source = ''
				
			names = item.getElementsByTagName('name')
			for items2 in names :
				for item2 in items2.childNodes :
					if self.is_nodeElement(item2):
						if item2.tagName == 'value' :
							orcomposite_name = item2.firstChild.nodeValue
						if item2.tagName == 'source' :	
							try:
								orcomposite_name_source = item2.firstChild.nodeValue
							except AttributeError:
								orcomposite_name_source = ''
			
			minimumversion = item.getElementsByTagName('minimumVersion')
			if len(minimumversion) > 0 :
				orcomposite_minimumversion = minimumversion[0].firstChild.nodeValue
			
			maximumversion = item.getElementsByTagName('maximumVersion')
			if len(maximumversion) :
				orcomposite_maximumversion = maximumversion[0].firstChild.nodeValue
				
								
			obj = Orcomposite(requirement_id
				, orcomposite_type
				, orcomposite_type_source
				, orcomposite_name
				, orcomposite_name_source
				, orcomposite_minimumversion
				, orcomposite_maximumversion
				, self.__now()
				, self.__now())
			self.commit(obj)		
		
	
	# ## educationals
	
	def handlerEducational(self, nodeO, lom_id) : 
		interactivitytype = nodeO.getElementsByTagName('interactivityType')
		educational_interactivitytype = ''
		educational_interactivitytype_source = ''
		if len(interactivitytype) > 0 :
			for items in interactivitytype :
				for item in items.childNodes :
					if self.is_nodeElement(item):
						if item.tagName == 'value' :
							educational_interactivitytype = item.firstChild.nodeValue
						if item.tagName == 'source' :	
							try:
								educational_interactivitytype_source = item.firstChild.nodeValue
							except AttributeError:
								educational_interactivitytype_source = ''
		
		interactivitylevel = nodeO.getElementsByTagName('interactivityLevel')
		educational_interactivitylevel = ''
		educational_interactivitylevel_source = ''
		if len(interactivitylevel) > 0 : 
			for items in interactivitylevel :
				for item in items.childNodes :
					if self.is_nodeElement(item):
						if item.tagName == 'value' :
							educational_interactivitylevel = item.firstChild.nodeValue
						if item.tagName == 'source' :	
							try:
								educational_interactivitylevel_source = item.firstChild.nodeValue
							except AttributeError:
								educational_interactivitylevel_source = ''
		
		semanticdensity = nodeO.getElementsByTagName('semanticDensity')
		educational_semanticdensity = ''
		educational_semanticdensity_source = ''
		if len(semanticdensity) > 0 :
			for items in semanticdensity :
				for item in items.childNodes :
					if self.is_nodeElement(item):
						if item.tagName == 'value' :
							educational_semanticdensity = item.firstChild.nodeValue
						if item.tagName == 'source' :	
							try:
								educational_semanticdensity_source = item.firstChild.nodeValue
							except AttributeError:
								educational_semanticdensity_source = ''
		
		difficulty = nodeO.getElementsByTagName('difficulty')
		educational_difficulty = ''
		educational_difficulty_source = ''
		if len(difficulty) :
			for items in difficulty :
				for item in items.childNodes :
					if self.is_nodeElement(item):
						if item.tagName == 'value' :
							educational_difficulty = item.firstChild.nodeValue
						if item.tagName == 'source' :
							try:
								educational_difficulty_source = item.firstChild.nodeValue
							except AttributeError:
								educational_difficulty_source = ''
		
		typicallearningtime = nodeO.getElementsByTagName('typicalLearningTime')
		educational_typicallearningtime = ''
		educational_typicallearningtime_description = ''
		educational_typicallearningtime_lang = ''
		if len(typicallearningtime) :
			for item1 in typicallearningtime.childNodes :
				if self.is_nodeElement(item1):
					if item1.tagName == 'duration' :
						educational_typicallearningtime = item1.firstChild.nodeValue

		obj = Educational(lom_id
			, educational_interactivitytype
			, educational_interactivitytype_source
			, educational_interactivitylevel
			, educational_interactivitylevel_source
			, educational_semanticdensity
			, educational_semanticdensity_source
			, educational_difficulty
			, educational_difficulty_source
			, educational_typicallearningtime
			, educational_typicallearningtime_description
			, educational_typicallearningtime_lang
			, self.__now()
			, self.__now())
		self.commit(obj)
	
		contexts = nodeO.getElementsByTagName('context')
		self.__handlerEducationalsContexts(contexts, obj.educational_id)
	
		learningResourceTypes = nodeO.getElementsByTagName('learningResourceType')
		self.__handlerEducationalsLearningresourcetypes(learningResourceTypes, obj.educational_id)
	
		intendedEndUserRoles = nodeO.getElementsByTagName('intendedEndUserRole')
		self.__handlerEducationalsUserroles(intendedEndUserRoles, obj.educational_id) 
	
		typicalAgeRange = nodeO.getElementsByTagName('typicalAgeRange')
		self.__handlerEducationalsTypicalagerange(typicalAgeRange, obj.educational_id)
	
		languages = nodeO.getElementsByTagName('language')
		self.__handlerEducationalsLanguages(languages, obj.educational_id)
	
		descriptions = nodeO.getElementsByTagName('description')
		self.__handlerEducationalsDescriptions(descriptions, obj.educational_id)
	
	def __handlerEducationalsDescriptions(self, nodeO, educational_id) :
		for items in nodeO:
			obj = EducationalsDescription(
				educational_id
				, self.__now()
				, self.__now())
			self.commit(obj)
			
			for item in items.childNodes :
				if self.is_nodeElement(item) and item.tagName == 'string':
					try:
						nodeO_string = item.firstChild.nodeValue
						nodeO_lang = ''
						if item.hasAttribute('language') :
							nodeO_lang = item.attributes['language'].value
						obj2 = EducationalsDescriptionsText(
							obj.educationals_description_id
							, nodeO_string
							, nodeO_lang
							, self.__now()
							, self.__now())
						self.commit(obj2)
					except AttributeError:
						pass
					
	def __handlerEducationalsLanguages(self, nodeO, educational_id) : 
		for item in nodeO: 
			educationals_language_string = item.firstChild.nodeValue
			obj = EducationalsLanguage(
				educational_id
				, educationals_language_string
				, self.__now()
				, self.__now())
			self.commit(obj)
	
	def __handlerEducationalsTypicalagerange(self, nodeO, educational_id) :
		for items in nodeO : 
			obj = EducationalsTypicalagerange(educational_id
				, self.__now()
				, self.__now())
			self.commit(obj)
			for item in items.childNodes :
				if self.is_nodeElement(item) and item.tagName == 'string':
					try:
						nodeO_string = item.firstChild.nodeValue
						nodeO_lang = ''
						if item.hasAttribute('language') :
							nodeO_lang = item.attributes['language'].value
						obj2 = EducationalsTypicalagerangesText(
							obj.educationals_typicalagerange_id
							, nodeO_string
							, nodeO_lang
							, self.__now()
							, self.__now())
						self.commit(obj2)
					except AttributeError:
						pass
					
	def __handlerEducationalsUserroles(self,  nodeO, educational_id) : 
		for items in nodeO:
			educationals_userrole_string = ''
			educationals_userrole_source = ''
			for item in items.childNodes :
				if self.is_nodeElement(item):
					if item.tagName == 'value' :
						educationals_userrole_string = item.firstChild.nodeValue
					if item.tagName == 'source' :
						try:
							educationals_userrole_source = item.firstChild.nodeValue
						except AttributeError:
							educationals_userrole_source = ''
				obj = EducationalsUserrole(
					educational_id
					, educationals_userrole_string
					, educationals_userrole_source
					, self.__now()
					, self.__now())
			self._session.add(obj)
		self._session.commit()	
	
	def __handlerEducationalsLearningresourcetypes(self, nodeO, educational_id) : 
		for items in nodeO:
			educationals_learningresourcetype_string = ''
			educationals_learningresourcetype_source = ''
			for item in items.childNodes :
				if self.is_nodeElement(item):
					if item.tagName == 'value' :
						educationals_learningresourcetype_string = item.firstChild.nodeValue
					if item.tagName == 'source' :
						try:
							educationals_learningresourcetype_source = item.firstChild.nodeValue
						except AttributeError:
							educationals_learningresourcetype_source = ''
				obj = EducationalsLearningresourcetype(
					educational_id
					, educationals_learningresourcetype_string
					, educationals_learningresourcetype_source
					, self.__now()
					, self.__now())
			self._session.add(obj)
		self._session.commit()
	
	def __handlerEducationalsContexts(self, nodeO, educational_id) :
		for items in nodeO:
			educationals_context_string= ''
			educationals_context_source = ''
			for item in items.childNodes :
				if self.is_nodeElement(item):
					if item.tagName == 'value' :
						educationals_context_string = item.firstChild.nodeValue
					if item.tagName == 'source' :
						try:
							educationals_context_source = item.firstChild.nodeValue
						except AttributeError:
							educationals_context_source = ''
				obj = EducationalsContext(
					educational_id
					, educationals_context_string
					, educationals_context_source
			     	, self.__now()
					, self.__now())
			self._session.add(obj)
		self._session.commit()
	
	# ## rights
	
	def handlerRight(self, right, lom_id) :
		cost = right.getElementsByTagName('cost')
		right_cost = ''
		right_cost_source = ''
		if len(cost) > 0 : 
			for items in cost :
				for item in items.childNodes :
					if self.is_nodeElement(item):
						if item.tagName == 'value' :
							right_cost = item.firstChild.nodeValue
						if item.tagName == 'source' :
							try:
								right_cost_source = item.firstChild.nodeValue
							except AttributeError:
								right_cost_source = ''

		copyrightI = right.getElementsByTagName('copyrightAndOtherRestrictions')
		right_copyright = ''
		right_copyright_source = ''
		if len(copyrightI) > 0 : 
			for items in copyrightI :
				for item in items.childNodes :
					if self.is_nodeElement(item):
						if item.tagName == 'value' :
							right_copyright = item.firstChild.nodeValue
						if item.tagName == 'source' :
							try:
								right_copyright_source = item.firstChild.nodeValue
							except AttributeError:
								right_copyright_source = ''
		
		obj = Right(
			lom_id
			, right_cost
			, right_cost_source
			, right_copyright
			, right_copyright_source
			, self.__now()
			, self.__now())
		self.commit(obj)
	
		descriptions = right.getElementsByTagName('description')
		self.__handlerRightsDescriptions(descriptions, obj.right_id)
	
	def	__handlerRightsDescriptions(self, nodeO, right_id) :
		for items in nodeO :
			for item in items.childNodes :
				if self.is_nodeElement(item) and item.tagName == 'string':
					try:
						nodeO_string = item.firstChild.nodeValue
						nodeO_lang = ''
						if item.hasAttribute('language') :
							nodeO_lang = item.attributes['language'].value
						
						obj = RightsDescription(
							right_id
							, nodeO_string
							, nodeO_lang
							, self.__now()
							, self.__now())
						self.commit(obj)
					except AttributeError:
						pass
					
	# ## annotation
	
	def handlerAnnotations(self, nodeO, lom_id) :
	
		aEntity = nodeO.getElementsByTagName('entity')
		annotation_entity = ''
		if len(aEntity) > 0 :
			for items in aEntity :
				for item in items.childNodes :
					annotation_entity = item.data
		date = nodeO.getElementsByTagName('date')
		annotation_date = ''
		annotation_date_description = ''
		annotation_date_description_lang = ''
		if len(date) > 0 :
			for items in date :
				for item in items.childNodes :
					if self.is_nodeElement(item) and item.tagName == 'dateTime':
						annotation_date = dateutil.parser.parse(item.firstChild.nodeValue)
		
		obj = Annotation(
			lom_id
			, annotation_entity
			, annotation_date
			, annotation_date_description
			, annotation_date_description_lang
			, self.__now()
			, self.__now())
		self.commit(obj)
		
		descriptions = nodeO.getElementsByTagName('description')
		self.__handlerAnnotationDescriptions(descriptions, obj.annotation_id)
	
	def	__handlerAnnotationDescriptions(self, nodeO, annotation_id) :
		for items in nodeO: 
			for item in items.childNodes :
				if self.is_nodeElement(item) and item.tagName == 'string':
					try:
						nodeO_string = item.firstChild.nodeValue
						nodeO_lang = ''
						if item.hasAttribute('language') :
							nodeO_lang = item.attributes['language'].value
					
						obj = AnnotationsDescription(
							annotation_id
							, nodeO_string
							, nodeO_lang
							, self.__now()
							, self.__now())
						self.commit(obj)
					except AttributeError:
						pass
	
	# ## classifications
	
	def handlerClassifications(self, nodeO, lom_id) :
		for item in nodeO :
			classification_purpose = ''	
			classification_purpose_source = ''
			keywords = ''
			descriptions = ''
			taxon = ''
			purpose = item.getElementsByTagName('purpose')
			for item0 in purpose :
				for item00 in item0.childNodes :
					if self.is_nodeElement(item00):
						if item00.tagName == 'value' :
							classification_purpose = item00.firstChild.nodeValue
						if item00.tagName == 'source' :
							try:
								classification_purpose_source = item00.firstChild.nodeValue
							except AttributeError:
								classification_purpose_source = ''
			
			keywords = item.getElementsByTagName('keyword')
			descriptions = item.getElementsByTagName('description')
			taxon =  item.getElementsByTagName('taxonPath')
						
			obj = Classification(
				lom_id
				, classification_purpose
				, classification_purpose_source
				, self.__now()
				, self.__now())
			self.commit(obj)
			self.__handlerClassificationsKeywords(keywords, obj.classification_id)
			self.__handlerClassificationsDescriptions(descriptions, obj.classification_id)				
			self.__handlerTaxonpaths(taxon, obj.classification_id)

	def __handlerClassificationsKeywords(self, nodeO, classification_id) :
		for items in nodeO:
			obj = ClassificationsKeyword(
				classification_id
				, self.__now()
				, self.__now())
			self.commit(obj)
			self.__handlerClassificationsKeywordsText(items, obj.classifications_keyword_id)
	
	def __handlerClassificationsKeywordsText(self, nodeO, classifications_keyword_id):
		for item in nodeO.childNodes :
			if self.is_nodeElement(item) and item.tagName == 'string':
				try:
					nodeO_string = item.firstChild.nodeValue
					nodeO_lang = ''
					if item.hasAttribute('language') :
						nodeO_lang = item.attributes['language'].value
		
					obj = ClassificationsKeywordsText(
						classifications_keyword_id
						, nodeO_string
						, nodeO_lang
						, self.__now()
						, self.__now())
					self.commit(obj)
				except AttributeError:
					pass
	
	def __handlerClassificationsDescriptions(self, nodeO, classification_id) :
		for items in nodeO:
			for item in items.childNodes :
				if self.is_nodeElement(item) and item.tagName == 'string':
					try:
						nodeO_string = item.firstChild.nodeValue
						nodeO_lang = ''
						if item.hasAttribute('language') :
							nodeO_lang = item.attributes['language'].value
			
						obj = ClassificationsDescription(
							classification_id
							, nodeO_string
							, nodeO_lang
							, self.__now()
							, self.__now())
						self.commit(obj)
					except AttributeError:
						pass
					
					
	# ## taxonpath
	
	def __handlerTaxonpaths(self, nodeO, classification_id) :
		for items in nodeO:
			obj = Taxonpath(
				classification_id
				, self.__now()
				, self.__now())
			self.commit(obj)
	
			taxons = items.getElementsByTagName('taxon')
			self.__handlerTaxons(taxons, obj.taxonpath_id)
		
			sources = items.getElementsByTagName('source')
			self.__handlerTaxonpathsSources(sources, obj.taxonpath_id)
		
	def __handlerTaxonpathsSources(self, nodeO, taxonpath_id) :
		for items in nodeO:
			for item in items.childNodes :
				if self.is_nodeElement(item) and item.tagName == 'string':
					try:
						nodeO_string = item.firstChild.nodeValue
						nodeO_lang = ''
						if item.hasAttribute('language') :
							nodeO_lang = item.attributes['language'].value
				
						obj = TaxonpathsSource(
							taxonpath_id
							, nodeO_string
							, nodeO_lang
							, self.__now()
							, self.__now())
						self.commit(obj)
					except AttributeError:
						pass
					
	def __handlerTaxons(self, taxons, taxonpath_id) :
		for taxon in taxons :
			taxon_id_string = ''
			for item in taxon.childNodes :
				if self.is_nodeElement(item) and item.tagName == 'id':
					taxon_id_string = item.firstChild.nodeValue
			obj = Taxon(
				taxonpath_id
				, taxon_id_string
				, self.__now()
				, self.__now())
			self.commit(obj)
		
			taxonentries = taxon.getElementsByTagName('entry')
			self.__handlerTaxonsEntries(taxonentries, obj.taxon_id)
	
	def __handlerTaxonsEntries(self, taxonentries, taxon_id) :
		for items in taxonentries:
			for item in items.childNodes :
				if self.is_nodeElement(item) and item.tagName == 'string':
					try:
						nodeO_string = item.firstChild.nodeValue 
						nodeO_lang = ''
						if item.hasAttribute('language') :
							nodeO_lang = item.attributes['language'].value
				
						obj = TaxonsEntry(
							taxon_id
							, nodeO_string
							, nodeO_lang
							, self.__now()
							, self.__now())
						self.commit(obj) 
					except AttributeError:
						pass
	
	def __now(self):
		return time.strftime('%Y-%m-%d %H:%M:%S')
	
	def commit(self, obj):
		self._session.add(obj)
		self._session.commit()
	
	def save(self):
		try :
			self._session.commit()
		except Exception, err:
			print '%s' % err
			self._session.rollback()
	
	def is_nodeElement(self, item):
		return item.nodeType == item.ELEMENT_NODE