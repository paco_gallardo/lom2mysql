import os, datetime

class Log(object):
		
	__file = 'lom2rdb.log'
	__handler = None
	
	def __init__(self) :
		if self.existFile() : 
			self.rename()
		self.open()
			
	def open(self) :
		self.__handler = open(self.__file, "a")
	
	def close(self) :
		self.__handler.close()
		
	def existFile(self) :
		return os.path.exists(self.__file)

	def rename(self) :
		os.rename(self.__file, self.__file + '_%s' % datetime.datetime.now().strftime("%Y-%m-%d_%H:%M:%S"))
		
	def write(self, id, query) :
		self.__handler.write('%s - %s\n' % (id,query))
        
	def message(self, msg) :
		self.__handler.write('MSG: %s\n' % (msg))
		print('MSG: %s' % (msg))

	def error(self, msg) :
		self.__handler.write('ERROR: %s\n' % (msg))
		print('ERROR: %s' % (msg))

	def info(self, msg) :
		self.__handler.write('INFO: %s\n' % (msg))
		print('INFO: %s' % (msg))