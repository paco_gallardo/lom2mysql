'''

	Paco Gallardo 2013

'''

from xml.dom.minidom import parse
import xml.parsers.expat

class Config :

	_path = 'conf.xml'
	_server = ''
	_db = ''
	_host = ''
	_port = ''
	_user = ''
	_password = ''

	dataFolder = ''
	engine = ''

	def __init__(self) :
		self.__get_config()
	
	def __get_config(self) :
		try : 
			dom = parse(self._path)
			
			dbConfig = dom.getElementsByTagName('mysql')[0]
			for item in dbConfig.childNodes :
				if item.nodeType == item.ELEMENT_NODE:
					if item.tagName == 'host':
						self._host = item.firstChild.nodeValue
					if item.tagName == 'port':
						self._port = item.firstChild.nodeValue
					if item.tagName == 'database':
						self._db = item.firstChild.nodeValue
					if item.tagName == 'user':
						self._user = item.firstChild.nodeValue
					if item.tagName == 'password':
						self._password = item.firstChild.nodeValue
			self.engine = 'mysql+mysqlconnector://%s:%s@%s:%s/%s' % (self._user,self._password, self._host,self._port,self._db)
			self.dataFolder = dom.getElementsByTagName('dataFolder')[0].firstChild.nodeValue
		except xml.parsers.expat.ExpatError:
			print 'Config error'
