#Prerequisites
	
* MySQL Python Connector
	- download from [http://dev.mysql.com/downloads/connector/python/](http://dev.mysql.com/downloads/connector/python/)

* Python SQL ORM - [http://www.sqlalchemy.org/]SQLAlchemy

#How to use it

* Conf.xml
 
```
<?xml version="1.0" encoding="UTF-8"?>
<conf>
    <mysql>
        <host>localhost</host>
        <port>3306</port>
        <database>IEEE-LOM</database>
        <user>user</user>
        <password>password</password>
    </mysql>
    <dataFolder>/path/to/xmls/</dataFolder>
</conf>

```

* Type:
```		
$ python lom2mysql.py
```