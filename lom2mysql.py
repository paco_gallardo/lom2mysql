'''

	Paco Gallardo 2013

'''

from conf import Config
from controller import Controller
from log import Log
from xml.dom import minidom
from xml.parsers.expat import ExpatError
import os, string


log = ''

def is_lom(dom):
	result = False
	son = ''
	check = {'general.identifier','general.language','technical.location','classification.purpose'}
	rC = 0
	for item in check : 
		var = string.split(item, '.')
		
		try : 
			if len(dom.getElementsByTagName(var[0])) > 0 :
				parent = dom.getElementsByTagName(var[0])[0]
				son = parent.getElementsByTagName(var[1])
			if len(son) > 0 :
				rC += 1
		except AttributeError:
			pass
	if rC == 4 :
		result = True
	else:
		log.error('it is not a IEEE-LOM file')
	return result
	
def read_document(fileName) :
	dom = ''
	try :
		dom = minidom.parse(fileName)
	except ExpatError:
		log.error('%s does not open' % fileName) 
	return dom
	
def main():
	config = Config()
	rootd = config.dataFolder
	for root, subFolders, files in os.walk(rootd) :
		for fileI in files :
			(drive, path1) = os.path.splitdrive(fileI)
			(path, filename) = os.path.split(path1)
			(fileN, fileExtension) = os.path.splitext(filename)
			lomFile = os.path.join(root, fileI)
			if fileExtension == '.xml' :
				dom = read_document(lomFile)
				log.info('Reading %s' % lomFile)
				if (is_lom(dom)) :
					controller = Controller()	
					lom_id = controller.handlerLom()
					
					if len(dom.getElementsByTagName('general')) > 0 :
						generals = dom.getElementsByTagName('general')[0]
						controller.handlerGenerals(generals, lom_id)
					if len(dom.getElementsByTagName('lifeCycle')) > 0 :
						lifeCycles = dom.getElementsByTagName('lifeCycle')[0]
						controller.handlerLifecycle(lifeCycles, lom_id) 
					if len(dom.getElementsByTagName('metaMetadata')) > 0 :
						metametadatas = dom.getElementsByTagName('metaMetadata')[0]
						controller.handlerMetametadatas(metametadatas, lom_id)
					if len(dom.getElementsByTagName('technical')) > 0 :
						technicals = dom.getElementsByTagName('technical')[0]
						controller.handlerTechnicals(technicals, lom_id)
					if len(dom.getElementsByTagName('educational')) > 0 :
						educationals = dom.getElementsByTagName('educational')[0]
						controller.handlerEducational(educationals, lom_id)
					if len(dom.getElementsByTagName('rights')) > 0 :
						rights = dom.getElementsByTagName('rights')[0]
						controller.handlerRight(rights, lom_id)
					if len(dom.getElementsByTagName('relation')) > 0 :
						relations = dom.getElementsByTagName('relation')[0]
						controller.handlerRelations(relations, lom_id)
					if len(dom.getElementsByTagName('annotation')) > 0 :
						annotations = dom.getElementsByTagName('annotation')[0]
						controller.handlerAnnotations(annotations, lom_id)
					if len(dom.getElementsByTagName('classification')) > 0 :
						classifications = dom.getElementsByTagName('classification')
						controller.handlerClassifications(classifications, lom_id)
				
					log.info('Saved with ID: %s' % lom_id )
					controller.save()				
				
if (__name__) == '__main__': 
	log = Log()
	main()
